Relációs Táblák
===

A tábla felépítése:  
![](res/sql00.jpg)

A tábla minden oszlopához meg van adva, hogy mi a neve, a mezői milyen típusú adatot tartalmazhat és hogy az érték lehet e NULL:  
![](res/sql01.jpg)


Adattípusok
===

|Category|Data Type|Example|Description|
|-|-|-|-|
|Numerikus|[BIT]()|`1`|0, 1, NULL|
|Numerikus|[TINYINT]()|`25`|0 és 255 közötti egész szám.|
|Numerikus|[INT]()|`254,421`|-2,147,483,648 és 2,147,483,647 közötti egész szám.|
|Numerikus|[BIGINT]()|`3,111,111,111`|-9,223,372,036,854,775,808 és 9,223,372,036,854,775,807 közötti egész szám.|
|Numerikus|[DECIMAL(precision,scale)]()|`135,123.541`|Max. 38 digites egész vagy tört szám.|
|Szöveges|[VARCHAR(max-length)]()|`'hello sql'`|Változó hosszságú szöveg.|
|Szöveges|[NVARCHAR]()|`'Árvíztűrő'`|Mint a VARCHAR, de Unicode karaktereket is tud tárolni.|
|Szöveges|[TEXT]()|`'hello sql'`|Az SQL Server 2005 előtt a VARCHAR-ba max. 8000 karakter fért. Ma már a TEXT típus elavult.|
|Dátum|[DATE]()|`'2018-12-03'`|Dátum|
|Dátum|[DATETIME]()|`'2018-12-03 15:19:11.123'`|Dátum és idő|
|Egyéb|[IMAGE]()|`'...'`|Max. 2.14 GB nagyságú bináris adat. Pl. fájlok tárolására.|
|Egyéb|[XML]()|`<unit id="123">`|XML adatokat tárol|

SELECT
===

```sql
SELECT
    <mezonev1>,
    <mezonev2>,
    ...
FROM <tablanev1>
WHERE <feltetelek>
ORDER BY
    <mezonev1> <DESC|ASC>,
    <mezonev2> <DESC|ASC>,
    ...
```

`SELECT` - rekord lekérdedzése:  
![](res/sql02_0.jpg)

`FROM` - rekordok lekérdezése táblából:  
![](res/sql02_1.jpg)

`WHERE` - rekordok szürése:  
![](res/sql02_2.jpg)

`ORDER BY` - rekordok rendezése:  
![](res/sql02_3.jpg)


JOIN
===

```sql
SELECT
    <alias1>.<mezonev1>,
    <alias1>.<mezonev2>,
    ...
FROM <tablanev1> <alias1>
JOIN <tablanev2> <alias2> ON <feltetelek>
JOIN ...
WHERE <feltetelek>
ORDER BY
    <mezonev1>,
    <mezonev2>,
    ...
```

![](res/sql03.jpg)

INSERT
===

```sql
INSERT INTO <tablanev> (<mezonev1>, <mezonev2>, ...)VALUES (<ertek1>, <ertek2>, ...)
```

![](res/sql04.jpg)

Megjegyzések:
- Az INSERT-ben az `ID` értéke nincs megadva, mert `auto_increment`-es.  
  (Ha megadnánk, hibaüzenetet adna: `"Cannot insert explicit value for identity column in table."`)
- Az INSERT-ben a `PartFamilyTypeID` értéke nincs megadva, mert az `Allow Nulls` be van kapcsolva, ezért opcionális.


FlexFlow tábla relációk
===

![](res/ff01.jpg)
