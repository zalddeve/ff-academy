SELECT ID, UnitStateID, StatusID, StationID, EmployeeID, CreationTime, LastUpdate, LineID, ProductionOrderID, PartID FROM ffUnit u 
SELECT ID, UnitID, UnitComponentTypeID, ChildUnitID, ChildSerialNumber, ChildPartID, InsertedEmployeeID, InsertedStationID, InsertedTime, RemovedEmployeeID, RemovedStationID, RemovedTime, StatusID FROM ffUnitComponent uc 
SELECT ID, [Description] FROM luUnitComponentType
SELECT ID, UnitID, EmployeeID, StationID, ExitTime, ProductionOrderID, PartID FROM ffHistory h 
SELECT ID, UserID, [Password], Lastname, Firstname, EmployeeGroupID, LastLogin FROM ffEmployee em 
SELECT ID, PartNumber, [Description], PartFamilyID, IsUnit FROM ffPart p 
SELECT ID, [Name], [Description] FROM luPartFamily pf 
SELECT ID, [Description] FROM ffUnitState us 
SELECT ID, ProductionOrderNumber, [Description], PartID, OrderQuantity, CreationTime, StatusID FROM ffProductionOrder po 
SELECT ID, [Description], LineQuantity, StartedQuantity, ReadyQuantity, LineID, ProductionOrderID FROM ffLineOrder
SELECT ID, SerialNumber, CreationTime, CurrentCount, StatusID FROM ffPackage pa 
SELECT ID, [Description] FROM luPackageStatus
SELECT ID, UnitID, InmostPackageID, OutmostPackageID, reserved_01, reserved_02, '...' [reserved_...] FROM ffUnitDetail ud 
SELECT ID, [Description] FROM ffLine l 
SELECT ID, [Description] FROM luUnitStatus
SELECT ID, [Description] FROM luProductionOrderStatus
SELECT ID, [Description], StationTypeID, LineID FROM ffStation s 
SELECT ID, [Description] FROM ffStationType
SELECT ID, PartID, PartDetailDefID, Content FROM ffPartDetail
SELECT ID, [Description] FROM luPartDetailDef
SELECT ID, UnitID, [Name], StationID, EmployeeID, CreationTime, IsPass, [Status] FROM ffTest
SELECT UnitID, SerialNumberTypeID, [Value] FROM ffSerialNumber sn 
SELECT ID, [Description] FROM luSerialNumberType snt
